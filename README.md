# Welcome to laravel 8 Test project

in this project i made a **Company** & **Employee** CRM.  for the authentication and login i have use **Jetstream/livewire** and all the styling are in **tailwindcss** also made seeder as requested  for the user email as admin@admin.com and password as 'password'
to run the migrations 

    php artisan migrate:fresh --seed 

to run this project 

     npm run build
     php artisan serve 
