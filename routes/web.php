<?php

use App\Http\Controllers\PageController;
use App\Http\Livewire\Companies;
use App\Http\Livewire\Employees;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function ()
{ return view('welcome');
});
Route::middleware(['auth:sanctum', 'verified'])->get('/company', Companies::class )->name('company');
Route::middleware(['auth:sanctum', 'verified'])->get('/employee', Employees::class )->name('employee');


Route::get('images/{filename}', function ($filename)
{
    $path = storage_path('app/public/images') . '/' . $filename;
    if(!File::exists($path)) abort(404);
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});