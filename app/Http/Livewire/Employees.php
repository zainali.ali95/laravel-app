<?php

namespace App\Http\Livewire;
use App\Models\Company;
use App\Models\Employee;
use Livewire\Component;

class Employees extends Component
{
    
    public $employees,$selectedID, $employee_id, $email, $firstname, $lastname, $companies_id,$companies,$phone;
    public $isOpen = 0;
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function render()
    {
        $this->employees = Employee::with('companies')->get();
       
        return view('livewire.employees');
    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function create()
    {
        $this->resetInputFields();
        $this->openModal();
    }

  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function openModal()
    {
        
        $this->companies = Company::all();
        $this->selectedID=  $this->companies[0]->id;
        $this->isOpen = true;
    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function closeModal()
    {
        $this->isOpen = false;
    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    private function resetInputFields(){
        $this->email = '';
        $this->firstname = '';
        $this->lastname = '';
        $this->phone = '';
        $this->companies_id = '';
    }
     
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function store()
    {
       

        
        $this->validate([
            
            'firstname' => 'required',
            'lastname' => 'required',
            'companies_id' => 'required',
           
        ]);
        
        // $url=storage_path($url);
        Employee::updateOrCreate(['id' => $this->employee_id], [
            
            'email' =>  $this->email,
            'firstname' =>  $this->firstname,
            'lastname' =>  $this->lastname,
            'phone' =>  $this->phone,
            'companies_id' =>  $this->companies_id,
            
        ]);
  
        session()->flash('message', 
            $this->employee_id ? 'Employee Updated Successfully.' : 'Employee Created Successfully.');
  
        $this->closeModal();
        $this->resetInputFields();
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function edit($id)
    {
        $employee = Employee::findOrFail($id);
        $this->employee_id = $employee->id;
        $this->email = $employee->email;
        $this->firstname = $employee->firstname;
        $this->lastname = $employee->lastname;
        $this->phone = $employee->phone;
        $this->companies_id = $employee->companies_id;
        
    
        $this->openModal();
    }
     
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function delete($id)
    {
        Employee::find($id)->delete();
        session()->flash('message', 'Employee Deleted Successfully.');
    }
    public function getCompany($id)
    {
        $company =Company::findOrFail($id);
      return $company;
    }
    
}
