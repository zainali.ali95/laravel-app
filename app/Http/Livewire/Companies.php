<?php

namespace App\Http\Livewire;

use App\Models\Company;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class Companies extends Component
{
    use WithFileUploads;
    public $companies, $email, $name, $company_id,$logo;
    public $isOpen = 0;
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function render()
    {
        $this->companies = Company::all();
        return view('livewire.companies');
    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function create()
    {
        $this->resetInputFields();
        $this->openModal();
    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function openModal()
    {
        $this->isOpen = true;
    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function closeModal()
    {
        $this->isOpen = false;
    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    private function resetInputFields(){
        $this->email = '';
        $this->name = '';
        $this->company_id = '';
    }
     
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function store()
    {
       

        
        $this->validate([
            'email' => 'required|email|unique:companies',
            'name' => 'required',
            'logo' => 'nullable|mimes:png,jpg,jpeg,csv,txt,pdf|max:2048'
        ]);
        $url = '';
        if($this->logo){
            $url = $this->logo->store('images','public')?$this->logo->store('images','public'):'';
        }
        
        // $url=storage_path($url);
        Company::updateOrCreate(['id' => $this->company_id], [
            'email' => $this->email,
            'name' => $this->name,
            'logo' =>  $url
        ]);
  
        session()->flash('message', 
            $this->company_id ? 'Company Updated Successfully.' : 'Company Created Successfully.');
  
        $this->closeModal();
        $this->resetInputFields();
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function edit($id)
    {
        $post = Company::findOrFail($id);
        $this->company_id = $id;
        $this->email = $post->email;
        $this->name = $post->name;
    
        $this->openModal();
    }
     
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function delete($id)
    {
        Company::find($id)->delete();
        session()->flash('message', 'Company Deleted Successfully.');
    }
}
