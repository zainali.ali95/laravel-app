<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Company;

class Employee extends Model
{
    use HasFactory;
    protected $fillable = [
        'firstname','lastname', 'email','phone','companies_id'
    ];
    protected $guarded = [];
    protected $table ="employees";
    public function companies(){
        
        return $this->belongsTo(Company::class, 'companies_id');
    }
  

    
}
