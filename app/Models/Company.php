<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Employee;

class Company extends Model
{
    use HasFactory;
    protected $table ="companies";
    
    protected $fillable = [
        'name', 'email','logo'
    ];
    public function employees(){
        //  return $this->hasMany('App\Post', name_of_column_in_post_table, name_of_column_in_user_table);
        return $this->hasMany(Employee::class);
    }
   
}
